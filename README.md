# Node-Natours
A mockup landing page for a travel agency startup.

Built using modern technologies: node.js, express.js, mongoDB, mongoose. 🙂

## Installation
$ npm i
$ npm run build:js
$ npm start

## Setting up ESLint and Prettier in VS Code
npm i eslint prettier eslint-config-prettier eslint-plugin-prettier eslint-config-airbnb eslint-plugin-node eslint-plugin-import eslint-plugin-jsx-a11y  eslint-plugin-react --save-dev

# Live:
Live project 👉 [Click](https://lakshman-natours.herokuapp.com/)